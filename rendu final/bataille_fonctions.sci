// Fichier de fonctions Scilab (.sci) pour le TP3



// ========================================================================
//          my_histc : Calcul d'histogramme
//
// remplacement "maison" à la fonction Scilab 'histc', qui est moins pratique à utiliser (à mon goût).

// INPUT :
//     * ax : axe des abscisses, correspondant aux frontières différentes classes de l'histogramme
//     * data : données à trier dans l'histogramme
// OUTPUT :
//     * cf : histogramme des données
//
// L'usage est globalement le même que celui de 'histc', à deux détails près :
//     * utilisation de la variable 'ax' un peu plus intuitive qu'avec 'histc'
//     * l'histogramme en sortie n'est pas normalisé. Il faut le faire après coup, si nécessaire.
// ========================================================================


function cf = my_histc(ax, data)

    // Version bourrine et lente
    // cf = zeros(length(ax), 1);
    // for n = 1:length(data)
    //     i = find(ax>=data(n), 1);
    //     if ~isempty(i) then
    //         cf(i) = cf(i)+1;
    //     end
    // end

    // Version scilab avec dsearch
    ax = [-%inf matrix(ax, 1, length(ax))];   // nouvel axe : assure un format ligne, rajoute une case à gauche
    [ind, cf] = dsearch(data, ax);

endfunction

/*
* createDeck generate a N cards game
*/
function deck= createDeck(N)
    // N number of cards
    nb=N/4; // number of cards per family
    deck= zeros(1,N);
    for i=1:N
        deck(i)=modulo(i,nb)+1;
    end
endfunction

/*
* removePlayedCards remove played cards from the deck
*/
function [player1, player2]= removePlayedCards(player1, player2, nbPlayed)
    if length(player1)<= nbPlayed then
        player1=[];
    else
        player1=player1((nbPlayed+1):$);
    end
    if length(player2)<= nbPlayed then
        player2=[];
    else
        player2=player2((nbPlayed+1):$);
    end
endfunction

/*
* gameInitialize divide the generated cards between two players
*/
function [player1,player2]= gameInitialize(N)
    deck= createDeck(N);
    deck= grand(1,'prm',deck);
    player1= deck(1:(N/2));
    player2= deck((N/2)+1:$);
    player1= grand(1,'prm',player1);
    player2= grand(1,'prm',player2);
endfunction

/*
* addCards give to the battle's winner the cards in table, order is randomized for a better game
*/
function deck= addCards(deck, table)
    if deck(1) == 0 then
        deck= grand(1,'prm',table);
    else
    deck=[deck,grand(1,'prm',table)];
    end
endfunction

/*
* easyBattle give victory to a random player
*/
function [player1, player2]= easyBattle(player1, player2, table)
    random=grand(1,'prm',(1:6));
    [table, player1, player2]= newRound(player1, player2, 2,table);
    if modulo(random(1),2) == 1 then
        // player1
        player1= addCards(player1, table);
        ////disp('//////// player 1 win //////// ');
    else
        // player2
        player2= addCards(player2, table);
        ////disp('//////// player 2 win //////// ');
    end
endfunction

/*
* normalBattle do a standard cards battle
*/
function [player1, player2]= normalBattle(player1, player2, table)
    tmp1 = player1(2);
    tmp2 = player2(2);
    [table, player1, player2]= newRound(player1, player2, 2, table);
    ////disp('player1:', tmp1, 'player2:', tmp2);
    if tmp1 > tmp2 then
        // player1
        player1= addCards(player1, table);
        ////disp('//////// player 1 win //////// ');
    elseif tmp2 > tmp1 then
        // player2
        player2= addCards(player2, table);
        ////disp('//////// player 2 win //////// ');
    elseif tmp1 == tmp2 then
        ////disp('//////// new battle ////////');
        try
            [player1, player2]= normalBattle(player1, player2, table);
        catch
                    if length(player2)<2 then
                        ////disp('//////// player 2 hasn''t enought cards //////// ');
                        inGame= %f;
                        break;
                    elseif length(player1)<2 then
                        ////disp('//////// player 1 hasn''t enought cards //////// ');
                        inGame= %f;
                        break;
                    else
                        error('new battle');
                    end
                end
    else
        error('normalBattle');
    end
    clear tmp1 tmp2;
endfunction

/*
* newRound play a new round
*/
function [table, player1, player2]= newRound(player1, player2, nbPlayed, table)
    table=[table, player1(1:nbPlayed), player2(1:nbPlayed)];
    [player1, player2]= removePlayedCards(player1, player2, nbPlayed);
endfunction

/*
* easyGame start and play a new game between two players with easy battle mode
*/
function [numberOfRound, winner]= easyGame(N)
    [player1,player2]= gameInitialize(N);
    inGame= %t;
    table=[];
    winner=0;
    numberOfRound=0;
    while inGame then
        if length(player1) == 0 then
            ////disp('//////// player 2 win the game //////// ');
            winner=2;
            inGame= %f;
            break;
        elseif length(player2) == 0 then
            ////disp('//////// player 1 win the game //////// ');
            winner=1;
            inGame= %f;
            break;
        else
            ////disp('-----new round-------');
            numberOfRound= numberOfRound +1;
            [table, player1, player2]= newRound(player1,player2,1, table);
            ////disp('player 1 :', table(1));
            ////disp('player 2 :',table(2));
            if table(1) < table(2) then                
                try
                    player2= addCards(player2, table);
                    ////disp('//////// player 2 win //////// ');
                catch
                    if length(player2)<2 then
                        ////disp('//////// player 1 win the game //////// ');
                        winner=1;
                        inGame= %f;
                        break;
                    elseif length(player1)<2 then
                        ////disp('//////// player 2 win the game //////// ');
                        winner=2;
                        inGame= %f;
                        break;
                    else
                        error('addCards player2');
                    end
                end
            elseif table(1) > table(2) then
                 try
                    player1= addCards(player1, table);
                    ////disp('//////// player 1 win //////// ');
                catch
                    if length(player2)<2 then
                        ////disp('//////// player 1 win the game //////// ');
                        winner=1;
                    elseif length(player1)<2 then
                        ////disp('//////// player 2 win the game //////// ');
                        winner=2;
                    else
                        error('addCards player1');
                    end
                end
            elseif table(1) == table(2) then
                ////disp('//////// easy battle ////////');
                try
                    [player1,player2]= easyBattle(player1, player2, table);
                catch
                    if length(player2)<2 then
                        ////disp('//////// player 2 hasn''t enought cards //////// ');
                        ////disp('//////// player 1 win the game //////// ');
                        winner=1;
                        inGame= %f;
                        break;
                    elseif length(player1)<2 then
                        ////disp('//////// player 1 hasn''t enought cards //////// ');
                        ////disp('//////// player 2 win the game //////// ');
                        winner=2;
                        inGame= %f;
                        break;
                    else
                        error('easy battle');
                    end
                end
            end
        end      
        table=[];
    end
    clear table;
endfunction

/*
* normalGame start and play a new game between two players with normal battle mode
*/
function [numberOfRound, winner]= normalGame(N)
    [player1,player2]= gameInitialize(N);
    inGame= %t;
    table=[];
    winner=0;
    numberOfRound=0;
    while inGame then
        if length(player1) == 0 then
            ////disp('//////// player 2 win the game //////// ');
            winner=2;
            inGame= %f;
            break;
        elseif length(player2) == 0 then
            ////disp('//////// player 1 win the game //////// ');
            winner=1;
            inGame= %f;
            break;
        else
            //disp('-----new round-------');
             numberOfRound= numberOfRound + 1;
            [table, player1, player2]= newRound(player1,player2,1, table);
            //disp('player 1 :', table(1));
            //disp('player 2 :',table(2));
            if table(1) < table(2) then                
                try
                    player2= addCards(player2, table);
                    //disp('//////// player 2 win //////// ');
                catch
                    if length(player2)<2 then
                        //disp('//////// player 1 win the game //////// ');
                        winner=1;
                        inGame= %f;
                        break;
                    elseif length(player1)<2 then
                        //disp('//////// player 2 win the game //////// ');
                        winner=2;
                        inGame= %f;
                        break;
                    else
                        error('addCards player2');
                    end
                end
            elseif table(1) > table(2) then
                 try
                    player1= addCards(player1, table);
                    //disp('//////// player 1 win //////// ');
                catch
                    if length(player2)<2 then
                        //disp('//////// player 1 win the game //////// ');
                        winner=1;
                    elseif length(player1)<2 then
                        //disp('//////// player 2 win the game //////// ');
                        winner=2;
                    else
                        error('addCards player1');
                    end
                end
            elseif table(1) == table(2) then
                //disp('//////// battle ////////');
                try
                    [player1,player2]= normalBattle(player1, player2, table);
                catch
                    if length(player2)<2 then
                        //disp('//////// player 2 hasn''t enought cards //////// ');
                        //disp('//////// player 1 win the game //////// ');
                        winner=1;
                        inGame= %f;
                        break;
                    elseif length(player1)<2 then
                        //disp('//////// player 1 hasn''t enought cards //////// ');
                        //disp('//////// player 2 win the game //////// ');
                        winner=2;
                        inGame= %f;
                        break;
                    else
                        error('battle');
                    end
                end
            end
        end      
        table=[];
    end
    clear table;
endfunction
