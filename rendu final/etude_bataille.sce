// florent chaplais G3

// Trois lignes utiles à mettre au début de chaque script :
xdel(winsid());                 // ferme toutes les fenetres a chaque nouvel appel du script
clear;                          // nettoie toutes les variables a chaque nouvel appel du script
mode(0);                        // pour afficher toutes les lignes ne terminant pas par ;


// Charge les fonctions du TP
exec("bataille_fonctions.sci", -1);


numberOfCards= input('enter the number of cards: '); // number of cards, must be /4. (32, 52, 78,....)
while modulo(numberOfCards,4)~=0 then
    printf('numberOfCards must be divisible by 4');
    numberOfCards= input('enter the number of cards: ');
end

N=input('enter sample size: '); // sample size

if %t then  // sample with easy battle mode
    timer();// cpu time
    tic();// time
    numberOfRound= zeros(N,1);
    winner= zeros(N,1);
    for i=1:N
        [numberOfRound(i,:), winner(i,:)]= easyGame(numberOfCards);
    end
    hist=my_histc([1,2],winner);
    disp('player1   player2');
    disp(hist/N);
    subplot(2,2,1);
    bar([1,2],hist/N);
    plot([0,3],[0.5 0.5], 'r');
    legends('50%',5,1);
    xtitle('probability of victory (easy)','players','victory');
    subplot(2,2,3);
    plot(numberOfRound);
    my_mean_easy= mean(numberOfRound)
    my_min_easy= min(numberOfRound)
    my_max_easy= max(numberOfRound)
    plot([0 N], [my_mean_easy my_mean_easy], 'red');
    plot([0 N], [my_min_easy my_min_easy], 'green');
    plot([0 N], [my_max_easy my_max_easy],'magenta');
    legends(['mean';'min';'max'],[color('red') color('green') color('magenta')],1);
    xtitle('number of round (easy)','N','round');
    disp('cpu time:');
    timer()// display cpu time
    disp('time:');
    toc()// display time
end

if %t then  // sample with normal battle mode
    timer();// cpu time
    tic();// time
    numberOfRound2= zeros(N,1);
    winner2= zeros(N,1);
    for i=1:N
        [numberOfRound2(i,:), winner2(i,:)]= normalGame(numberOfCards);
    end
    hist2=my_histc([1,2],winner2);    
    disp('player1   player2');
    disp(hist2/N);
    subplot(2,2,2);
    bar([1,2],hist2/N);
    plot([0,3],[0.5 0.5], 'r');
    legends('50%',5,1);
    xtitle('probability of victory (normal)','players','victory');
    subplot(2,2,4);
    plot(numberOfRound2);
    my_mean_normal= mean(numberOfRound2)
    my_min_normal= min(numberOfRound2)
    my_max_normal= max(numberOfRound2)
    plot([0 N], [my_mean_normal my_mean_normal], 'red');
    plot([0 N], [my_min_normal my_min_normal], 'green');
    plot([0 N], [my_max_normal my_max_normal],'magenta');
    legends(['mean';'min';'max'],[color('red') color('green') color('magenta')],1);
    xtitle('number of round (normal)','N','round');
    disp('cpu time:');
    timer()// display cpu time
    disp('time:');
    toc()// display time
end


clear;
